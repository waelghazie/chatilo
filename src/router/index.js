import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store/index";

Vue.use(VueRouter);
import page404 from "./../views/auth/page404";
import loginRoutes from "@/views/auth/routes.js";
import groupRoutes from "@/views/group/routes.js";
import chatRoutes from "@/views/chat/routes.js";

const routes = [
  ...loginRoutes,
  ...groupRoutes,
  ...chatRoutes,
  {
    path: "/",
    redirect: { name: "chat" },
    meta: {
      title: "Home",
      roles: ["guest", "user", "admin"]
    }
  },
  {
    path: "*",
    name: "page404",
    component: page404,
    meta: {
      title: "login",
      roles: ["guest", "user", "admin"]
    }
  }
];

const router = new VueRouter({
  mode: "history",
  // base: process.env.BASE_URL,
  routes
});

router.beforeEach(async (to, from, next) => {
  if (to.meta.roles.includes(store.state.auth.role)) return next();
  else return next({ name: "error-403" });
});

const DEFAULT_TITLE = "Chatilo";

router.afterEach(to => {
  Vue.nextTick(() => {
    document.title = to.meta.title || DEFAULT_TITLE;
  });
  window.scrollTo(0, 0);
});

export default router;
