import promise from "promise";
import api from "@/api/chat.api.js";
import { getField, updateField } from "vuex-map-fields";

export default {
  namespaced: true,
  state: {
    selectedGroup: null,
    messages: [],
    newMessage: null,
    groups: [],
    groupTypes: [],
    groupFollowed: []
  },

  getters: {
    getField
  },

  mutations: {
    updateField,
    SET_MESSAGES(state, data) {
      state.messages = data;
    },
    SET_GROUPS(state, data) {
      state.groups = data.groups;
      state.groupTypes = data.groupTypes;
      state.groupFollowed = data.groupFollowed;
    },
    CLEAR_MESSAGE(state) {
      state.newMessage = "";
    }
  },

  actions: {
    getGroupDetails({ commit }, userId) {
      return new promise((resolve, reject) => {
        api
          .getGroupDetails(userId)
          .then(res => {
            commit("SET_GROUPS", res);
            resolve();
          })
          .catch(err => reject(err));
      });
    },
    getMessages({ state, commit }) {
      return new promise((resolve, reject) => {
        api
          .getMessages(state.selectedGroup.group_id, state.messages.length)
          .then(res => {
            commit("SET_MESSAGES", res);
            resolve();
          })
          .catch(err => reject(err));
      });
    },
    followGroup({ state }) {
      return new promise((resolve, reject) => {
        api
          .followGroup(state.selectedGroup.group_id)
          .then(() => {
            resolve();
          })
          .catch(err => reject(err));
      });
    },
    insertMessage({ state, commit, dispatch }) {
      return new promise((resolve, reject) => {
        let data = {
          group_id: state.selectedGroup.group_id,
          content: state.newMessage.substring(0, 29)
        };
        api
          .insertMessage(data)
          .then(() => {
            dispatch("getMessages");
            commit("CLEAR_MESSAGE");
            resolve();
          })
          .catch(err => reject(err));
      });
    },
    deleteMessage({ dispatch }, id) {
      return new promise((resolve, reject) => {
        api
          .deleteMessage(id)
          .then(() => {
            dispatch("getMessages");
            resolve();
          })
          .catch(err => reject(err));
      });
    }
  }
};
