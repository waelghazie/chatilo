import promise from "promise";
import api from "@/api/group.api.js";
import { getField, updateField } from "vuex-map-fields";

export default {
  namespaced: true,
  state: {
    //group
    id: null,
    name: null,
    groupTypeId: null,
    maximumMembers: null,
    groupErrors: [],
    // groups
    groups: [],
    headers: [
      { text: "Name", value: "name", align: "center", sortable: true },
      {
        text: "Type",
        value: "type",
        align: "center",
        sortable: true
      },
      {
        text: "Max members",
        value: "maximum_members",
        align: "center",
        sortable: true
      },
      { text: "Actions", value: "actions", align: "center", sortable: false }
    ],
    //groupTypes
    types: []
  },

  getters: {
    getField
  },

  mutations: {
    updateField,
    SET_GROUP(state, data) {
      state.id = data.id;
      state.name = data.name;
      state.groupTypeId = data.group_type_id;
      state.maximumMembers = data.maximum_members;
    },
    SET_GROUPS(state, data) {
      state.groups = data;
    },
    RESET_GROUP(state) {
      state.id = 0;
      state.name = null;
      state.groupTypeId = null;
      state.maximumMembers = null;
    },
    SET_TYPES(state, data) {
      state.types = data;
    },
    SET_GROUP_ERRORS(state, data) {
      for (const key in data) state.groupErrors.push(data[key][0]);
    }
  },

  actions: {
    loadGroup({ commit, state }) {
      return new promise((resolve, reject) => {
        api
          .show(state.id)
          .then(res => {
            commit("SET_GROUP", res.data);
            resolve();
          })
          .catch(err => reject(err));
      });
    },
    loadGroups({ commit }) {
      return new promise((resolve, reject) => {
        api
          .index()
          .then(res => {
            commit("SET_GROUPS", res);
            resolve();
          })
          .catch(err => {
            reject(err);
          });
      });
    },
    deleteGroup({ dispatch }, id) {
      return new promise((resolve, reject) => {
        api
          .delete(id)
          .then(() => {
            dispatch("loadGroups");
            resolve();
          })
          .catch(err => reject(err));
      });
    },
    loadTypes({ commit }) {
      api.types().then(res => commit("SET_TYPES", res));
    },
    createGroup({ state, commit }) {
      return new promise((resolve, reject) => {
        let data = {
          name: state.name,
          group_type_id: state.groupTypeId,
          maximum_members: state.maximumMembers
        };
        api
          .create(data)
          .then(() => {
            resolve();
          })
          .catch(err => {
            commit("SET_GROUP_ERRORS", err.response.data);
            reject();
          });
      });
    },
    updateGroup({ state, commit }) {
      return new promise((resolve, reject) => {
        let data = {
          name: state.name,
          group_type_id: state.groupTypeId,
          maximum_members: state.maximumMembers
        };
        api
          .update(data, state.id)
          .then(() => {
            resolve();
          })
          .catch(err => {
            commit("SET_GROUP_ERRORS", err.response.data);
            reject();
          });
      });
    }
  }
};
