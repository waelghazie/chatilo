import axios from "@/plugins/api";
import promise from "promise";
import api from "@/api/auth.api.js";
import { getField, updateField } from "vuex-map-fields";

export default {
  namespaced: true,
  state: {
    token: localStorage.getItem("token") ? localStorage.getItem("token") : null,
    userId: localStorage.getItem("userId")
      ? localStorage.getItem("userId")
      : null,
    role: localStorage.getItem("role") ? localStorage.getItem("role") : "guest"
  },

  getters: {
    getField,
    isLoggedIn(state) {
      return state.token !== null;
    }
  },

  mutations: {
    updateField,
    LOGIN(state, data) {
      state.token = data.token;
      state.userId = data.user_id;
      state.role = data.user_type;
    },
    LOGOUT(state) {
      state.token = null;
      state.userId = null;
      state.role = "guest";
    }
  },

  actions: {
    login({ commit }, credentials) {
      return new promise((resolve, reject) => {
        api
          .login(credentials.email, credentials.password)
          .then(res => {
            const token = res.data.token;
            localStorage.setItem("token", token);
            localStorage.setItem("userId", res.data.user_id);
            localStorage.setItem("role", res.data.user_type);
            commit("LOGIN", res.data);
            axios.defaults.headers.common["Authorization"] = "Bearer " + token;
            resolve();
          })
          .catch(err => reject(err));
      });
    },
    logout({ commit }) {
      return new promise((resolve, reject) => {
        api
          .logout()
          .then(() => {
            resolve();
          })
          .catch(() => {
            reject();
          })
          .finally(() => {
            localStorage.removeItem("token");
            localStorage.removeItem("userId");
            localStorage.removeItem("role");
            commit("LOGOUT");
          });
      });
    }
  }
};
