import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
import auth from "./modules/auth.store";
import group from "./modules/group.store";
import chat from "./modules/chat.store";

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    group,
    chat
  }
});
