import Vue from "vue";
import Vuetify from "vuetify/lib";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        secondary: "#caeaf7",
        textColor: "#4f5052",
        inputBack: "#caeaf7"
      }
    }
  }
});
