const chat = () => import("./show.vue");

export default [
  {
    path: "/chat",
    name: "chat",
    component: chat,
    meta: {
      title: "Chat",
      roles: ["user", "admin"]
    }
  }
];
