const groups = () => import("./index.vue");

export default [
  {
    path: "/groups",
    name: "groups",
    component: groups,
    meta: {
      title: "Groups",
      roles: ["admin"]
    }
  }
];
