const login = () => import("./login.vue");
const logout = () => import("./logout.vue");
const register = () => import("./register.vue");
const page403 = () => import("./page403.vue");

export default [
  {
    path: "/auth/login",
    name: "login",
    component: login,
    meta: {
      title: "login",
      roles: ["guest"]
    }
  },
  {
    path: "/auth/logout",
    name: "logout",
    component: logout,
    meta: {
      title: "logout",
      roles: ["user", "admin", "guest"]
    }
  },
  {
    path: "/auth/register",
    name: "register",
    component: register,
    meta: {
      title: "register",
      roles: ["guest"]
    }
  },
  {
    path: "/error-403",
    name: "error-403",
    component: page403,
    meta: {
      title: "error",
      type: "show",
      roles: ["guest", "user", "admin"]
    }
  }
];
