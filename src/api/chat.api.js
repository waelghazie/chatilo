import axios from "@/plugins/api";

export default {
  async getGroupDetails(userId) {
    const res = await axios.get(`/api/getGroupDetails?user_id=${userId}`);
    return res.data;
  },

  async getMessages(groupId, offset) {
    const res = await axios.get(
      `/api/getMessages?group_id=${groupId}&offset=${offset}`
    );
    return res.data;
  },

  async insertMessage(data) {
    const res = await axios.post(`/api/insertMessage`, data);
    return res.data;
  },

  async deleteMessage(id) {
    const res = await axios.delete(`/api/deleteMessage/${id}`);
    return res.data;
  },

  async followGroup(id) {
    const res = await axios.post(`/api/followGroup`, { group_id: id });
    return res.data;
  },

  async unfollowGroup(id) {
    const res = await axios.post(`/api/unfollowGroup`, { group_id: id });
    return res.data;
  }
};
