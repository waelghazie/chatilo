import axios from "@/plugins/api";

export default {
  async login(email, password) {
    const res = await axios.post("/api/login", {
      email: email,
      password: password
    });
    return res;
  },

  async logout() {
    const res = await axios.post("/api/logout");
    return res;
  },

  async register(data) {
    const res = await axios.post("/api/user", data);
    return res;
  }
};
