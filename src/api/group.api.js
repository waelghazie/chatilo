import axios from "@/plugins/api";

export default {
  async index() {
    const res = await axios.get("/api/group");
    return res.data.map(item => {
      return {
        ...item,
        type: item.type.name
      };
    });
  },

  async show(id) {
    const res = await axios.get(`/api/group/${id}`);
    return res;
  },

  async create(data) {
    const res = await axios.post("/api/group", data);
    return res;
  },

  async udpate(data, id) {
    const res = await axios.post(`/api/group/${id}`, data);
    return res;
  },

  async delete(id) {
    const res = await axios.delete(`/api/group/${id}`);
    return res;
  },

  async types() {
    const res = await axios.get(`/api/getGroupTypes`);
    return res.data;
  }
};
