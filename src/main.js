import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
// import pusher from "./plugins/pusher";

Vue.config.productionTip = false;

import Notification from "vue-notification";
Vue.use(Notification);

import Vuelidate from "vuelidate";
Vue.use(Vuelidate);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
